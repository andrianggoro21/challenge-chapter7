'use strict';
const {
  Model
} = require('sequelize');

//import bcrypt untuk melakukan enkripsi
const bcrypt = require ('bcrypt');
const jwt = require('jsonwebtoken');

module.exports = (sequelize, DataTypes) => {
  class user_games extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }

    
    //register
    static #encrypt = (password) => bcrypt.hashSync(password, 10)

    static register = ({username, password}) => {
      const encryptedPassword = this.#encrypt(password)

      return this.create ({username, password : encryptedPassword})
    }
    
    //Login
    checkPassword = password => bcrypt.compareSync(password, this.password)


    // Metode JWT
    generateToken = () => {
      const payload = {
        id: this.id,
        username: this.username,
      }
       const rahasia = 'rahasia'

       const token = jwt.sign(payload, rahasia)
       return token;
    }

  
    static authenticate = async({ username, password }) => {
      try {
        const user = await this.findOne({where: {username}})
        if (!user) return Promise.reject("User not found")

        const isPasswordValid = user.checkPassword(password)
        if (!isPasswordValid) return Promise.reject("Wrong password")
        return Promise.resolve(user)
      }
      catch(err){
        return Promise.reject(err)
      }
    }
    //metode local
    // static authenticate = async ({ username, password }) => {
    //   try {
    //     const user = await this.findOne({where: {username}})
    //     if (!user) return Promise.reject("User not found")
    //     const isPasswordValid = user.checkPassword(password)
    //     if (!isPasswordValid) return Promise.reject("Wrong password")
    //     return Promise.resolve(user)
    //   }
    //   catch(err){
    //     return Promise.reject(err)
    //   }
    // }
  };
  user_games.init({
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    issuperadmin: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'user_games',
  });
  return user_games;
};