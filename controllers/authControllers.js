const {user_games} = require ('../models');
const passport = require ('passport');
const jwt = require('jsonwebtoken');




// tambahan JWT
function format (user){
    const {id, username, issuperadmin} = user
    return {
        id,
        username,
        issuperadmin,
        accesssToken : user.generateToken()
    } 
}
module.exports = {
    register: (req, res, next) => {
        user_games.register(req.body).then (() => {
            res.redirect('/login')
        })
        .catch (err => next (err))
    },
    login: (req, res) => {
        user_games.authenticate(req.body)
        .then(user => {
            res.json(format(user))
        })
    },
    whoami:(req, res) => {
        user_games.findAll().then((result) => {
                res.json(result);
            })
        
        // const currentUser = req.user;
        // res.json(currentUser)
    }
}




// Pakai Local ini saja tidak tambahan jwt
// module.exports = {
//     register: (req, res, next) => {

//         user_games.register(req.body).then (() => {
//             res.redirect('/login')
//         })
//         .catch (err => next (err))
//     },
//     login: passport.authenticate('local', {
//         successRedirect: '/users',
//         failureRedirect: '/login',
//         failureFlash: true // Untuk mengaktifkan express flash
//     }),    
// }
 