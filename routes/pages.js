var express = require('express');
var router = express.Router();
const auth = require('../controllers/authControllers');
const restrict = require('../middlewares/restrict');
const roleMiddleware = require('../middlewares/roleMiddleware')


// router.get('/register', (req, res) => res.render('register'));
router.post('/register', auth.register);

// router.get('/login', (req, res) => res.render('login'));
router.post('/login', auth.login);

router.get('/whoami', restrict, roleMiddleware.access, auth.whoami);

router.post('/room');

router.post('/room/fight');




module.exports = router;