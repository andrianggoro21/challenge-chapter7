var express = require('express');
var router = express.Router();
const restrict = require('../middlewares/restrict');

/* GET users listing. */
router.get('/', restrict, function(req, res, next) {
  res.send('respond with a resource');
});

module.exports = router;
