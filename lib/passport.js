const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy
const { Strategy: JwtStrategy, ExtractJwt} = require ('passport-jwt');
const { user_games } = require('../models');
const jwt = require('jsonwebtoken');

const options = {
    jwtFromRequest : ExtractJwt.fromHeader ('authorization' ),

    secretOrKey : 'rahasia',
}

passport.use(new JwtStrategy(options, async (payload, done) => {

    user_games.findByPk(payload.id)
        .then(user => done (null, user))
        .catch(err => done (err, false))
}))

// passport local
// async function authenticate(username, password, done){
//     try {
//         const user = await user_games.authenticate({ username, password});
//         return done(null, user);
//     }
//     catch(err){
//         return done(null, false, {message: err.message})
//     }
// }

// passport.use(
//     new LocalStrategy({ usernameField:'username', passwordField:'password'}, authenticate)
// )

// passport.serializeUser(
//     (user, done) => done(null, user.id)
// )

// passport.deserializeUser(
//     async (id, done) => done(null, await user_games.findByPk(id))
// )

module.exports = passport;